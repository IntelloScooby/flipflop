package com.ascendon.base

import com.ascendon.domain.Client

interface WebServiceClient {

    fun retrieveAllClients() : List<Client>
}