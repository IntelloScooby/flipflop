package com.ascendon.domain

data class Client(val clientName: String, val clientLogoResourceId: Int, var isClientCardSolved: Boolean)