package com.ascendon.domain

/**
 * isCurrentRoundComplete -> openCards = 2 then send true else false
 * areOpenCardsEqual -> both client names are same then send true else false
 * isGameOver -> totalCorrectCards = MAX_CARDS_TO_PLAY then send true else false
 */

class GameBoardController(private val allAvailableClientCards: List<Client>) {

    private val TAG = GameBoardController::class.java.simpleName

    private val MAX_CARDS_TO_PLAY = allAvailableClientCards.size

    private var previousCardIndex: Int? = null
    private var currentCardIndex: Int? = null

    private companion object {
        var totalCorrectCards = 0
        var currentRoundCardCounter: Int = 0
    }

    fun isCurrentRoundComplete(): Boolean {
        currentRoundCardCounter++
        return currentRoundCardCounter == 2
    }

    fun areOpenCardsEqual(): Boolean {
        resetCounter()
        if (allAvailableClientCards[previousCardIndex!!].clientName == allAvailableClientCards[currentCardIndex!!].clientName) {
            totalCorrectCards += 2
            return true
        }
        return false
    }

    fun isGameOver(): Boolean {
        return totalCorrectCards == MAX_CARDS_TO_PLAY
    }

    private fun resetCounter() {
        currentRoundCardCounter = 0
    }

    fun setCardIndices(previousCardIndex: Int?, currentCardIndex: Int?){
        this.previousCardIndex = previousCardIndex
        this.currentCardIndex = currentCardIndex
    }
}