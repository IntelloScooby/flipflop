package com.ascendon.domain

interface FlipTimer {

    fun onTimerEnded()

    fun onTimerStarted()
}