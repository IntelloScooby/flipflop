package com.ascendon.flipflop

import com.ascendon.base.WebServiceClient
import com.ascendon.domain.Client

class MockClient : WebServiceClient {

    override fun retrieveAllClients(): List<Client> {
        return listOf(Client("Disney", 1, false)
                , Client("TalkTalk", 2, false)
                , Client("Redbox", 3, false)
                , Client("Formula 1", 4, false)
                , Client("Marvel", 5, false)
                , Client("Disney", 1, false)
                , Client("TalkTalk", 2, false)
                , Client("Redbox", 3, false)
                , Client("Formula 1", 4, false)
                , Client("Marvel", 5, false))
    }
}