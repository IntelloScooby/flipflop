package com.ascedon.flipflop

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.support.v7.widget.CardView
import kotlinx.android.synthetic.main.card_view.view.*

class CardAnimation(private val context: Context) : CardAdapter.CardFlipper {
    private lateinit var mSetRightOut: AnimatorSet
    private lateinit var mSetLeftIn: AnimatorSet
    private val mDistance = 8000
    private val mScale = context.resources.displayMetrics.density * mDistance

    override fun onOpenCard(cardView: CardView) {
        loadAnimation(cardView)
        cardView.isClickable = false
        mSetRightOut.setTarget(cardView.card_back)
        mSetLeftIn.setTarget(cardView.card_front)
        mSetRightOut.start()
        mSetLeftIn.start()
    }

    override fun onCloseCard(cardView: CardView) {
        loadAnimation(cardView)
        cardView.isClickable = true
        mSetRightOut.setTarget(cardView.card_front)
        mSetLeftIn.setTarget(cardView.card_back)
        mSetRightOut.start()
        mSetLeftIn.start()
    }

    private fun loadAnimation(cardView: CardView) {
        val mCardFrontLayout = cardView.card_front
        val mCardBackLayout = cardView.card_back
        mCardFrontLayout.cameraDistance = mScale
        mCardBackLayout.cameraDistance = mScale
        mSetRightOut = AnimatorInflater.loadAnimator(context, R.animator.out_animation) as AnimatorSet
        mSetLeftIn = AnimatorInflater.loadAnimator(context, R.animator.in_animation) as AnimatorSet
    }
}