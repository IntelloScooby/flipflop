package com.ascedon.flipflop

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.card_back_view.view.*
import kotlinx.android.synthetic.main.card_front_view.view.*
import kotlinx.android.synthetic.main.card_view.view.*

class CardAdapter(cardListener: CardClickListener, gameBoardViewModel: GameBoardViewModel, cardAnimation: CardAnimation) : RecyclerView.Adapter<CardAdapter.CardViewHolder>() {
    private val TAG: String = this.javaClass.simpleName

    private val mAllClientsList = GameBoardViewModel.getAllClientsList()

    private val mCardClickListener: CardClickListener = cardListener
    private val mCardAnimation: CardAnimation = cardAnimation
    private val mViewModel = gameBoardViewModel

    private lateinit var mContext: Context

    interface CardClickListener {
        fun onCardClick(cardPosition: Int)
    }

    interface CardFlipper {
        fun onOpenCard(cardView: CardView)
        fun onCloseCard(cardView: CardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CardAdapter.CardViewHolder? {
        if (parent != null) {
            mContext = parent.context
        }
        return CardViewHolder(LayoutInflater.from(parent?.context)
                .inflate(R.layout.card_view, parent, false) as CardView)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        setupCardView(holder.cardView, position)
        if (mAllClientsList[position].isClientCardSolved) {
            keepCardOpen(holder.cardView)
        } else {
            mCardAnimation.onCloseCard(holder.cardView)
        }
    }

    override fun getItemCount(): Int {
        return mAllClientsList.size
    }

    fun updateGameBoardAtEndOfRound() {
        val cardIndices = mViewModel.getCardsAtEndOfRound()
        cardIndices[0]?.let { notifyItemChanged(it) }
        cardIndices[1]?.let { notifyItemChanged(it) }
    }

    private fun setupCardView(cardView: CardView, position: Int) {
        cardView.card_back.ascendon_logo.text = "0"
        cardView.card_front.client_name.text = mAllClientsList[position].clientName
        cardView.card_front.client_logo.text = mAllClientsList[position].clientLogoResourceId.toString()
    }

    private fun keepCardOpen(cardView: CardView) {
        cardView.isClickable = false
        cardView.setOnClickListener(null)
        cardView.card_front.card_front_id.setBackgroundColor(mContext.resources.getColor(R.color.colorAccent))
    }

    inner class CardViewHolder(internal val cardView: CardView) : RecyclerView.ViewHolder(cardView), View.OnClickListener {

        init {
            cardView.setOnClickListener(this)
        }

        override fun onClick(card: View?) {
            mCardAnimation.onOpenCard(card as CardView)
            mCardClickListener.onCardClick(adapterPosition)
        }
    }
}