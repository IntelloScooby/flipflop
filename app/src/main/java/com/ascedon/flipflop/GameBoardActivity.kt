package com.ascedon.flipflop

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.activity_game_board.*

class GameBoardActivity : AppCompatActivity(), CardAdapter.CardClickListener {

    private val TAG = this.javaClass.simpleName

    private lateinit var cardRecycler: RecyclerView
    private lateinit var cardAdapter: CardAdapter
    private lateinit var gameBoardViewModel: GameBoardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_board)
        gameBoardViewModel = ViewModelProviders.of(this).get(GameBoardViewModel::class.java)

        setupRecyclerView()

        gameBoardViewModel.getIsRoundOver().observe(this, Observer<Boolean> { roundOver ->
            if (roundOver != null && roundOver) {
                cardAdapter.updateGameBoardAtEndOfRound()
            }
        })

        gameBoardViewModel.getIsGameOver().observe(this, Observer<Boolean> { gameOver ->
            if (gameOver != null && gameOver) {
                displayGameOverMessage()
            }
        })
    }

    private fun setupRecyclerView() {
        cardRecycler = findViewById(R.id.card_recycler)
        cardAdapter = CardAdapter(this, gameBoardViewModel, cardAnimation = CardAnimation(this))
        cardRecycler.adapter = cardAdapter
        cardRecycler.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.grid_columns))
        cardRecycler.setHasFixedSize(true)
    }

    override fun onCardClick(cardPosition: Int) {
        gameBoardViewModel.onClickCheckGameBoard(cardPosition)
    }

    private fun displayGameOverMessage() {
        game_over_message_view.text = resources.getString(R.string.game_over_message)
        card_recycler.visibility = INVISIBLE
        game_over_message_view.visibility = VISIBLE
    }
}
