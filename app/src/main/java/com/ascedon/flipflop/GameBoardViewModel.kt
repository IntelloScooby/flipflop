package com.ascedon.flipflop

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ascendon.domain.Client
import com.ascendon.domain.GameBoardController

class GameBoardViewModel : ViewModel() {

    private val TAG = GameBoardViewModel::class.java.simpleName

    private var firstCardIndex: Int? = null
    private var secondCardIndex: Int? = null

    private val isGameOver = MutableLiveData<Boolean>()
    private val isRoundOver = MutableLiveData<Boolean>()

    private val gameBoardController = GameBoardController(allAvailableClientCards)
    private var cardsAtEndOfRound = listOf<Int?>()
    private var cardsAreEqual = false

    companion object {
        private val allAvailableClientCards = FlipFlopApplication().client.retrieveAllClients()
        fun getAllClientsList(): List<Client> {
            return allAvailableClientCards
        }
    }

    fun onClickCheckGameBoard(clickedCardPosition: Int) {
        setClickedCardIndex(clickedCardPosition)
        gameBoardController.setCardIndices(firstCardIndex, secondCardIndex)

        if (gameBoardController.isCurrentRoundComplete()) {
            compareOpenCards()
            updateGameOverValue()
            resetCardIndices()
            isRoundOver.value = true
        } else {
            isRoundOver.value = false
        }
    }

    fun getIsGameOver(): MutableLiveData<Boolean> {
        if (isGameOver.value == null) {
            isGameOver.value = false
        }
        return isGameOver
    }

    fun getIsRoundOver(): MutableLiveData<Boolean> {
        if (isRoundOver.value == null) {
            isRoundOver.value = false
        }
        return isRoundOver
    }

    fun getCardsAtEndOfRound(): List<Int?> {
        return cardsAtEndOfRound
    }

    private fun setClickedCardIndex(clickedCardPosition: Int) {
        //        If firstCardIndex is null it means its the first card of the round else, its the second card of the round
        if (firstCardIndex == null) {
            firstCardIndex = clickedCardPosition
        } else {
            secondCardIndex = clickedCardPosition
        }
    }

    private fun compareOpenCards() {
        cardsAreEqual = if (gameBoardController.areOpenCardsEqual()) {
            updateCardSolvedInClient()
            true
        } else {
            false
        }
    }

    private fun resetCardIndices() {
        val firstCard = firstCardIndex
        val secondCard = secondCardIndex

        cardsAtEndOfRound = listOf(firstCard, secondCard)

        firstCardIndex = null
        secondCardIndex = null
    }

    private fun updateCardSolvedInClient() {
        allAvailableClientCards[firstCardIndex!!].isClientCardSolved = true
        allAvailableClientCards[secondCardIndex!!].isClientCardSolved = true
    }

    private fun updateGameOverValue() {
        isGameOver.value = gameBoardController.isGameOver()
    }
}