package com.ascedon.flipflop

import android.app.Application
import com.ascendon.base.WebServiceClient
import com.ascendon.flipflop.FlipFlopWebServiceClient
import com.ascendon.flipflop.MockClient

class FlipFlopApplication : Application() {

    internal val client : WebServiceClient

    init {
//        client = FlipFlopWebServiceClient() // MockClient
        client = MockClient()
    }
//    val imageLibrary : WebServiceClient
//    val authenticationService : AuthService

    override fun onCreate() {
        super.onCreate()

        configureClient()
//        imageLibrary = FlipFlopImageLibrary
//        authenticationService = FlipFlopAuthService
    }

    fun getNetworkClient() : WebServiceClient
    {
        return client
    }

    fun configureClient()
    {

    }
}