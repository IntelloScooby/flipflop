package com.ascedon.flipflop

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.widget.ImageView
import android.widget.TextView
import com.ascendon.domain.Client

class Card(context: Context?) : ConstraintLayout(context) {

    var frontFacing : Boolean = false
//    val logoImageView : ImageView
    val titleTextView : TextView?
    val descriptionTextView : TextView?

    init {
//        logoImageView = rootView.logo
        titleTextView = null
        descriptionTextView = null
    }

    open fun setModel(client : Client)
    {

    }
}